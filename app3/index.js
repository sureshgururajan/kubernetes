const express = require('express')
const app = express()
const port = 3000

app.get('/app3', (req, res) => {
  res.send('Welcome to app3')
})

app.listen(port, () => {
  console.log(`app3 listening at http://localhost:${port}`)
})